#!/bin/sh

set -e

FBSDVER=10.3
_FBSDVER=`echo "${FBSDVER}" | sed 's|[.]|_|g'`
__FBSDVER=`echo "${FBSDVER}" | sed 's|[.]||g'`

MNAME=`basename "${0}"`
MDIR=`dirname "${0}"`

if [ ${#} -ne 1 ]
then
  echo "usage: ${MNAME} SVN_REV" >&2
  exit 1
fi
SVN_REV="${1}"
for x in "${MDIR}"/*
do
   if [ "${x}" = "${0}" ]
   then
     continue
   fi
   git rm -rf "${x}"
   if [ -e "${x}" ]
   then
     rm -rf "${x}"
   fi
done
cp -Rp /usr/srcs/${__FBSDVER}/* "${MDIR}/"
git add -A "${MDIR}"/*
git commit -m "Update with FreeBSD tree checked out from releng/${FBSDVER}, revision ${SVN_REV}."
git tag FreeBSD.RELENG_${_FBSDVER}.${SVN_REV}
echo git push origin
echo git push origin FreeBSD.RELENG_${_FBSDVER}.${SVN_REV}
